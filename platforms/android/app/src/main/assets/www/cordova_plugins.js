cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognition",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognition.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognition"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognitionError",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognitionError.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognitionError"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognitionAlternative",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognitionAlternative.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognitionAlternative"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognitionResult",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognitionResult.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognitionResult"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognitionResultList",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognitionResultList.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognitionResultList"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechRecognitionEvent",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechRecognitionEvent.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechRecognitionEvent"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechGrammar",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechGrammar.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechGrammar"
      ]
    },
    {
      "id": "phonegap-plugin-speech-recognition.SpeechGrammarList",
      "file": "plugins/phonegap-plugin-speech-recognition/www/SpeechGrammarList.js",
      "pluginId": "phonegap-plugin-speech-recognition",
      "clobbers": [
        "SpeechGrammarList"
      ]
    },
    {
      "id": "cordova-plugin-ble-central.ble",
      "file": "plugins/cordova-plugin-ble-central/www/ble.js",
      "pluginId": "cordova-plugin-ble-central",
      "clobbers": [
        "ble"
      ]
    },
    {
      "id": "phonegap-plugin-speech-synthesis.SpeechSynthesis",
      "file": "plugins/phonegap-plugin-speech-synthesis/www/SpeechSynthesis.js",
      "pluginId": "phonegap-plugin-speech-synthesis",
      "clobbers": [
        "window.speechSynthesis"
      ]
    },
    {
      "id": "phonegap-plugin-speech-synthesis.SpeechSynthesisUtterance",
      "file": "plugins/phonegap-plugin-speech-synthesis/www/SpeechSynthesisUtterance.js",
      "pluginId": "phonegap-plugin-speech-synthesis",
      "clobbers": [
        "SpeechSynthesisUtterance"
      ]
    },
    {
      "id": "phonegap-plugin-speech-synthesis.SpeechSynthesisEvent",
      "file": "plugins/phonegap-plugin-speech-synthesis/www/SpeechSynthesisEvent.js",
      "pluginId": "phonegap-plugin-speech-synthesis",
      "clobbers": [
        "SpeechSynthesisEvent"
      ]
    },
    {
      "id": "phonegap-plugin-speech-synthesis.SpeechSynthesisVoice",
      "file": "plugins/phonegap-plugin-speech-synthesis/www/SpeechSynthesisVoice.js",
      "pluginId": "phonegap-plugin-speech-synthesis",
      "clobbers": [
        "SpeechSynthesisVoice"
      ]
    },
    {
      "id": "phonegap-plugin-speech-synthesis.SpeechSynthesisVoiceList",
      "file": "plugins/phonegap-plugin-speech-synthesis/www/SpeechSynthesisVoiceList.js",
      "pluginId": "phonegap-plugin-speech-synthesis",
      "clobbers": [
        "SpeechSynthesisVoiceList"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.3",
    "phonegap-plugin-speech-recognition": "0.3.0",
    "cordova-plugin-ble-central": "1.2.2",
    "phonegap-plugin-speech-synthesis": "0.1.1"
  };
});