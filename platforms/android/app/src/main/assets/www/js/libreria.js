const UUID_service = 'be940000-7333-be46-b7ae-689e71722bd5';
const UUID = 'be940001-7333-be46-b7ae-689e71722bd5';
const UUID3 = 'be940003-7333-be46-b7ae-689e71722bd5';
var heartRate = {
	service: '180d',
	measurement: '2a37'
};


// String con las frases de la conversacion con el asistente de voz
var bot1 = 'Hola, ¿te encuentras bien?';
var bot2 = 'Me alegro mucho que estes bien. Hasta luego!';
var bot3 = '¿Estas seguro de que te encuentras bien?';
var bot4 = 'No se encuentra bien. Enviaré una alarma';

var user1 = 'si me encuentro bien';
var user11 = 'si';
var user12 = 'sí';
var user13 = 'me encuentro bien';
var user14 = 'sí me encuentro bien';

var user2 = 'seguro que me encuentro bien';
var user22 = 'seguro';

var user3 = 'no me encuentro bien'
var user31 = 'no'
var user32 = 'estoy mal'
var user33 = 'me encuentro mal'

var device;

var ictus = {

	buscarpulseras(listarDispositivos) {


		// si el bluetooth no esta activado se pide al usuario que lo active
		ble.enable(onSucces, this.onFailure);

		function onSucces() {
			// buscará solo dispositivos que tienen el servicio de heartRate
			ble.scan([heartRate.service], 15, listadoPulseras, this.onFailure);

			function listadoPulseras(pulseras) {
				console.log(pulseras.id);

				listarDispositivos(pulseras);

			}
		}
	},

	conectar(deviceId) {

		ble.connect(deviceId, onConnect, this.onFailure);

		function onConnect() {

			console.log("Connected to " + deviceId);
			device = deviceId;
			ble.stopScan(function () {

				console.log("Stop Scan Devices");

			},
				ictus.onFailure);
		}
	},

	desconectar() {
		ble.disconnect(device, function () { console.log("Desconectado"); }, this.onFailure);
	},

	medirSalud(funcion) {

		//Enviando array para iniciar la medicion en la pulsera...
		var data = new Uint8Array([3, 2, 7, 0, 2, 100, -73]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);
		console.log("Leyendo datos de salud...");

		var data2 = new Uint8Array([3, 9, 9, 0, 1, 3, 5, 20, -5]);

		ble.writeWithoutResponse(device, UUID_service, UUID, data2.buffer, this.onSucces, this.onFailure);

		//starNotificacion para actualizar los datos cada vez que cambian (tener en cuenta en UUID char es el tercero 'be940003-7333-be46-b7ae-689e71722bd5')

		//externalFunction("Hasta luego");

		ble.startNotification(device, UUID_service, UUID3, function (e) {

			var data = new Uint8Array(e);

			var arraySalud = {
				'sistolica': data[4],
				'distolica': data[5],
				'pulsaciones': data[6]
			};

			funcion(arraySalud);

		}, this.onFailure);
	},

	medirDeporte(funcion) {

		console.log("Leyendo datos de deporte...");

		//se envia el primer array para abrir canal de comunicacion 
		var data = new Uint8Array([3, 2, 7, 0, 0, 38, -105]);
		ble.writeWithoutResponse(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);

		//se envia el segundo array para empezar a leer y recibir los datos
		var data2 = new Uint8Array([3, 9, 9, 0, 1, 0, 3, -127, -50]);
		ble.writeWithoutResponse(device, UUID_service, UUID, data2.buffer, this.onSucces, this.onFailure);

		//cada vez que cambien los datos seran enviados 
		ble.startNotification(device, UUID_service, UUID3, function (e) {

			//se pasa el array de bytes a Hex y se envia a la vista
			var data = new Uint16Array(e);

			var arrayDeporte = {
				'pasos': data[2],
				'km': data[3] / 1000,
				'kcal': data[4]
			};

			funcion(arrayDeporte);

		}, this.onFailure);

	},

	pararMedicion() {

		var data = new Uint8Array([3, 2, 7, 0, 0, 38, -105]);

		ble.write(device, UUID_service, UUID, data.buffer, this.onSucces, this.onFailure);
		ble.stopNotification(device, UUID_service, UUID3);
	},

	onSucces(e) {
		console.log(e);
	},

	onFailure(err) {
		console.log('Error: ' + JSON.stringify(err));

	},

	asistenteVoz() {
		// Crear instancia de reconocimiento de voz 
		var recognition = new SpeechRecognition();
		recognition.lang = 'es-ES';
		recognition.continuous = false;
		recognition.interimResults = false;
		recognition.maxAlternatives = 1;

		// Crear instacias para leer el texto
		var u = new SpeechSynthesisUtterance();

		u.text = bot1;
		u.lang = 'es-ES';
		u.pitch = 1.8; // Tono
		u.rate = 1; // Velocidad 
	/* 	var voces = speechSynthesis.getVoices();
		u.voice = voces[8]; */

		u.onend = function () {
			recognition.start();
		}

		var me_alegro = new SpeechSynthesisUtterance();
		me_alegro.text = bot2;
		me_alegro.lang = 'es-ES';
		me_alegro.pitch = 1.8;
		me_alegro.rate = 1;

		var alarma = new SpeechSynthesisUtterance();
		alarma.text = bot4;
		alarma.lang = 'es-ES';
		alarma.pitch = 1.8;
		alarma.rate = 1;

		var seguro = new SpeechSynthesisUtterance();
		seguro.text = bot3;
		seguro.lang = 'es-ES';
		seguro.pitch = 1.8;
		seguro.rate = 1;
		seguro.onend = function () {
			recognition.start();
		}

		var respuesta = new String('');

		speechSynthesis.speak(u);

		recognition.onresult = function (event) {

			if (event.results.length > 0) {

				respuesta = event.results[0][0].transcript;

				console.log('La respuesta en el asistente de voz es: ' + respuesta);

			}

			switch (respuesta) {

				case user1:
				case user11:
				case user12:
				case user13:
				case user14:

					console.log('Esta bien');
					speechSynthesis.speak(seguro);
					
					break;

				case user2:
				case user22:

					console.log('Seguro que esta bien');
					speechSynthesis.speak(me_alegro);

					break;

				case user3:
				case user31:
				case user32:
				case user33:

					console.log('No se encuentra bien');
					speechSynthesis.speak(alarma);

					break;

				default:

					console.log('default switch case');

					break;
			}
		}
	},

	/**
	 * @param {*} frase  Frase a leer 
	 * 
	 * Devuelve true si se lee la frase correctamente, sino devuelve false.
	 */
	disfasia(frase) {

		var frase_grabada;
		var res;

		// Crear instancia de reconocimiento de voz 
		var recognition = new SpeechRecognition();
		recognition.lang = 'es-ES';
		recognition.continuous = false;
		recognition.interimResults = false;
		recognition.maxAlternatives = 1;

		recognition.start();

		recognition.onresult = function (event) {

			if (event.results.length > 0) {

				frase_grabada = event.results[0][0].transcript;

				if (frase_grabada == frase) {
					console.log('Frase Correcta');
					console.log( frase_grabada + ' ' + frase );
					res = true;
				} else {
					console.log('Frase Incorrecta');
					console.log( frase_grabada + ' ' + frase );

					res = false;

				}

			}
		}

		return res;
	},
}

