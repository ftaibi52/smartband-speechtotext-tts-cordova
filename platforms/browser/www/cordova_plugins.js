cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-ble-central/www/ble.js",
        "id": "cordova-plugin-ble-central.ble",
        "pluginId": "cordova-plugin-ble-central",
        "clobbers": [
            "ble"
        ]
    },
    {
        "file": "plugins/cordova-plugin-ble-central/src/browser/BLECentralPlugin.js",
        "id": "cordova-plugin-ble-central.BLECentralPlugin",
        "pluginId": "cordova-plugin-ble-central",
        "merges": [
            "ble"
        ]
    },
    {
        "file": "plugins/phonegap-plugin-speech-recognition/www/browser/SpeechRecognition.js",
        "id": "phonegap-plugin-speech-recognition.SpeechRecognition",
        "pluginId": "phonegap-plugin-speech-recognition",
        "runs": true
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-compat": "1.2.0",
    "cordova-plugin-ble-central": "1.2.2",
    "cordova-plugin-whitelist": "1.3.3",
    "phonegap-plugin-speech-recognition": "0.3.0",
    "phonegap-plugin-speech-synthesis": "0.1.1"
}
// BOTTOM OF METADATA
});