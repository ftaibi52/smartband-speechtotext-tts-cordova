/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {

    // Application Constructor
    initialize: function () {
        this.iniciarBotones();

    },

    iniciarBotones: function () {
        var buscar = document.getElementById('buscar');
        var desconectar = document.getElementById('desconectar');
        var medir = document.getElementById('medir');
        var deporte = document.getElementById('deporte');
        var stop = document.getElementById('stop');
        var voz = document.getElementById('voz');
        var deviceList = document.getElementById('deviceList');
        var dis = document.getElementById('dis');

        dis.addEventListener('touchstart', this.disfasia, false);
        buscar.addEventListener('touchstart', this.buscar, false);
        desconectar.addEventListener('touchstart', this.desconectar, false);
        medir.addEventListener('touchstart', this.medir, false);
        deporte.addEventListener('touchstart', this.deporte, false);
        stop.addEventListener('touchstart', this.stop, false);
        voz.addEventListener('touchstart', this.asistenteVoz, false);
        deviceList.addEventListener('touchstart', this.conectar, false); // assume not scrolling

    },

    asistenteVoz: function () {

        ictus.asistenteVoz();
    },

    disfasia: function () {

        // Recibe como parámetro lo frase que tiene que leer el paciente (todo en múscula y sin tildes)
        var frase = 'hola qué tal';

        var res = null;
        // Devolverá true si el paciente lee la frase correctamente 
        res = ictus.disfasia(frase);

        if (res != null) {

            if (res) {

                console.log('Lectura correcta');

            } else {

                console.log('Lectura Incorrecta');

            }
        }

    },

    buscar: function () {

        // Limpio todos los resultados de la lista para poner los nuevos
        deviceList.innerHTML = '';

        ictus.buscarpulseras(listarDispositivos);

        function listarDispositivos(devices) {

            //crea el boton en la vista
            var listItem = document.createElement('button'),
                html = '<b>' + devices.name + '</b><br/>' +
                    'RSSI: ' + devices.rssi + '&nbsp;|&nbsp;' +
                    devices.id;

            listItem.dataset.deviceId = devices.id;  // TODO
            listItem.innerHTML = html;
            deviceList.appendChild(listItem);

        }
    },

    conectar: function (e) {

        var deviceId = e.target.dataset.deviceId;

        ictus.conectar(deviceId);

    },

    desconectar: function () {
        ictus.desconectar();

    },

    medir: function () {

        //Función básica que imprime el resultado de la medición
        function funcion(arraySalud) {
            console.log('El resultado es: ', arraySalud.sistolica + ' ' + arraySalud.distolica + ' ' + arraySalud.pulsaciones);
        }

        //Se envia la peticion para que la pulsera empice a medir y la funcion con la que vamos a tratar los datos como parámetro
        ictus.medirSalud(funcion);
    },

    deporte: function () {

        //Función básica que imprime el resultado de la medición
        function funcion(arrayDeporte) {
            console.log('El resultado es: ', arrayDeporte.pasos + ' ' + arrayDeporte.km + ' ' + arrayDeporte.kcal);
        }

        //Se envia la peticion para que la pulsera empice a medir y la funcion con la que vamos a tratar los datos como parámetro
        ictus.medirDeporte(funcion);
    },

    stop: function () {
        ictus.pararMedicion();
    },

};

app.initialize();


